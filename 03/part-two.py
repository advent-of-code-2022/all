#!/usr/bin/env python3

import os

def answer(input_file):
    def get_score_from_intersection(intersection_set):
        char_val = ord(list(intersection_set).pop())
        return (char_val - 96) if char_val >= 97 else (char_val - 38)

    with open(input_file, "r") as input:
        data = [line.strip() for line in input.readlines()]

    score = sum([get_score_from_intersection(set.intersection(*[set(bag) for bag in group])) for group in [data[i:i + 3] for i in range(0, len(data), 3)]])

    print(f"Answer: Score is *** {score} ***")
    
input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
