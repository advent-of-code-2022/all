#!/usr/bin/env python3

import os

class FSNode:
    serial_number = -1

    fs_table = {}

    def __init__(self, parent, name, size):
        FSNode.serial_number += 1
        self.parent = None if not parent else parent.serial_number
        self.serial_number = FSNode.serial_number
        self.name = name
        self.path = name if not parent else f"{parent.path}{name}"
        self.size = size
        self.children = {}
        if self.serial_number > 0:
            FSNode.fs_table[self.parent].children[self.name]=self.serial_number
        FSNode.fs_table[self.serial_number] = self
    
    def get(serial_number):
        return FSNode.fs_table[serial_number]

    def get_size(self):
        if self.size:
            return self.size
        elif len(self.children) > 0:
            self.size = sum(map(lambda child: FSNode.fs_table[child].get_size(), self.children.values()))
            return self.size
        else:
            return 0

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    current_node = FSNode(None, "/", None)

    for line in data:
        if line[0] == "$":
            if line[1:5] == " cd ":
                cd_path = line[5:]
                if cd_path == "/":
                    current_node = FSNode.get(0)
                elif cd_path == "..":
                    current_node = FSNode.get(current_node.parent)
                else:
                    current_node = FSNode.get(current_node.children[f"{cd_path}/"])
        else:
            i,n = line.split(" ")
            if i == "dir":
                FSNode(current_node, f"{n}/", None)
            else:
                FSNode(current_node, n, int(i))
    
    total_size = FSNode.fs_table[0].get_size()
    remaining_space = 70000000 - total_size
    need_to_delete = 30000000 - remaining_space

    dir_sizes = [node.size for node in FSNode.fs_table.values() if node.name[-1] == "/" and node.size >= need_to_delete]
    print(min(dir_sizes))

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
