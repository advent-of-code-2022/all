#!/usr/bin/env python3

import os
from math import prod, lcm

class Monkey:
    
    monkeys = {}

    _operand_functions = {
        "+": lambda x,y: x+y,
        "*": lambda x,y: x*y
    }

    _lcm = 0

    def __init__(self, data):
        input_data = data.split("\n")
        self.id = int(input_data[0].strip(":").split(" ")[1])
        self.items = [int(v) for v in input_data[1].split(": ")[1].split(", ")]
        self.test_divisor = int(input_data[3].split(" ")[-1])
        self.target = {
            True: int(input_data[4].split(" ")[-1]),
            False:int(input_data[5].split(" ")[-1])
        }
        self.operand_function = Monkey._operand_functions[input_data[2].split(" ")[-2]]
        self.parameter = input_data[2].split(" ")[-1]
        self.inspection_count = 0
        Monkey.monkeys[self.id] = self
        Monkey._lcm = lcm(*[monkey.test_divisor for monkey in Monkey.monkeys.values()])

    def take_turn(self):
        while len(self.items) > 0:
            item = self.items.pop()
            self.inspection_count += 1
            parameter = item if self.parameter == "old" else int(self.parameter)
            worry = self.operand_function(item, parameter)
            target = self.target[worry % self.test_divisor == 0]
            Monkey.monkeys[target].items.append(worry % Monkey._lcm)

def answer(input_file):
    with open(input_file, "r") as input_data:
        monkey_data = input_data.read().split("\n\n")

    [Monkey(monkey) for monkey in monkey_data]

    for _ in range(0,10000):
        for monkey in Monkey.monkeys.values():
            monkey.take_turn()

    answer = prod(sorted([monkey.inspection_count for monkey in Monkey.monkeys.values()])[-2:])

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
