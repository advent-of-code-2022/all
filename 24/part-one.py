#!/usr/bin/env python3

from dataclasses import dataclass
from typing import List
import os

@dataclass()
class State:
    step: int
    x: int
    y: int

@dataclass()
class Blizzard:
    id: int
    x: int
    y: int
    min_x: int
    min_y: int
    max_x: int
    max_y: int
    vector_x: int
    vector_y: int

    def move(self):
        new_x = self.x + self.vector_x 
        new_y = self.y + self.vector_y
        if new_x > self.max_x:
            new_x = self.min_x
        if new_x < self.min_x:
            new_x = self.max_x
        if new_y > self.max_y:
            new_y = self.min_y
        if new_y < self.min_y:
            new_y = self.max_y
        return Blizzard(self.id, new_x, new_y, self.min_x, self.min_y, self.max_x, self.max_y, self.vector_x, self.vector_y)

class Blizzards:

    @classmethod
    def from_raw_data(cls, data):
        id = 0
        blizzards = []
        min_x, max_x = 1, len(data[0]) - 2
        min_y, max_y = 1, len(data) - 2
        for iy in range(min_y, max_y + 1):
            for ix in range(min_x, max_x + 1):
                value = data[iy][ix]
                vector_x, vector_y = 0, 0
                if value == ">":
                    vector_x = 1
                elif value == "v":
                    vector_y = 1
                elif value == "<":
                    vector_x = -1
                elif value == "^":
                    vector_y = -1
                if vector_x + vector_y != 0:
                    blizzards.append(Blizzard(id, ix, iy, min_x, min_y if (vector_y == 0 or ix != min_x) else min_y-1, max_x, max_y if (vector_y == 0 or ix != max_x) else max_y+1, vector_x, vector_y))
                    id += 1
        return cls(blizzards, min_x, min_y, max_x, max_y)

    def __init__(self, blizzards: List[Blizzard], min_x: int, min_y: int, max_x: int, max_y: int):
        self.min_x = min_x
        self.min_y = min_y
        self.max_x = max_x
        self.max_y = max_y
        self.x = {}
        for blizzard in blizzards:
            if blizzard.x not in self.x:
                self.x[blizzard.x] = {}
            if blizzard.y not in self.x[blizzard.x]:
                self.x[blizzard.x][blizzard.y] = []
            self.x[blizzard.x][blizzard.y].append(blizzard)

    def move(self):
        return Blizzards([b.move() for y in self.x.values() for ba in y.values() for b in ba], self.min_x, self.min_y, self.max_x, self.max_y)

    def options(self, x, y):
        option_list = [(-1,0), (0, -1), (0,0), (1, 0), (0, 1)]
        checked_options = [self._check_option(x, y, option) for option in option_list]
        return [option for option in checked_options if option]
    
    def _check_option(self, x, y, option):
        check_x = x + option[0]
        check_y = y + option[1]
        if self.min_x <= check_x <= self.max_x and self.min_y <= check_y <= self.max_y:
            if check_x not in self.x:
                return (check_x, check_y)
            if check_y not in self.x[check_x]:
                return (check_x, check_y)
            return None
        elif check_x == self.max_x and check_y == self.max_y + 1:
            return(check_x, check_y)
        elif check_x == self.min_x and check_y == self.min_y - 1:
            return(check_x, check_y)
        else:
            return None

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    blizzards = {0:Blizzards.from_raw_data(data)}
    for b in range(1, len(data)*2):
        blizzards[b] = blizzards[b-1].move()

    states_processed = set()
    states = [State(0,1,0)]
    winning_state = None

    while len(states) > 0:
        state = states.pop()
        if winning_state and winning_state.step < state.step:
            continue
        if state.step+1 not in blizzards:
            blizzards[state.step+1] = blizzards[state.step].move()
        states_processed.add((state.step * 10000000) + (state.x * 1000) + state.y)
        options = blizzards[state.step + 1].options(state.x, state.y)
        if len(options) == 0:
            continue
        else:
            next_step_value = ((state.step+1) * 10000000)
            for option in options:
                if (next_step_value + (option[0] * 1000) + option[1]) not in states_processed:
                    if option[1] == len(data)-1:
                        if winning_state is None or winning_state.step > (state.step + 1):
                            winning_state = State(state.step + 1, option[0], option[1])
                        break
                    states.append(State(state.step + 1, option[0], option[1]))

    answer = winning_state.step

    print(f"The answer is *** {answer} ***")    

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
