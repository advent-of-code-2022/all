#!/usr/bin/env python3

import os
import re

def answer(input_file):
    with open(input_file, "r") as input:
        data = [[int(value) for value in re.split(",|-", line.strip())] for line in input.readlines()]

    count = 0
    for line in data:
        count += 1 if len(range(max(line[0], line[2]), min(line[1], line[3]) + 1)) > 0 else 0
    
    print(f"Answer: Count is *** {count} ***")
    
input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
