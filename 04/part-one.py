#!/usr/bin/env python3

import os
import re

def answer(input_file):
    with open(input_file, "r") as input:
        data = [[int(value) for value in re.split(",|-", line.strip())] for line in input.readlines()]

    count = 0
    for line in data:
        if (line[0]>=line[2] and line[0]<=line[3] and line[1]<=line[3]) or (line[2]>=line[0] and line[2]<=line[1] and line[3]<=line[1]):
            count += 1
    
    print(f"Answer: Count is *** {count} ***")
    
input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
