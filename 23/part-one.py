#!/usr/bin/env python3

import os

class Elf:
    x = {}
    y = {}

    def can_move():
        return [e for y in Elf.x.values() for e in y.values() if e._can_move()]

    def count():
        return len([e for y in Elf.x.values() for e in y.values()])

    def for_all(func):
        [func(e) for y in Elf.x.values() for e in y.values()]

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.neighbours = None
        self._add_to_lists()

    def _add_to_lists(self):
        if self.x in Elf.x:
            Elf.x[self.x][self.y] = self
        else:
            Elf.x[self.x] = {self.y: self}
        if self.y in Elf.y:
            Elf.y[self.y][self.x] = self
        else:
            Elf.y[self.y] = {self.x: self}

    def move_to(self, new_x, new_y):
        self.neighbours = None
        Elf.x[self.x].pop(self.y)
        Elf.y[self.y].pop(self.x)
        self.x = new_x
        self.y = new_y
        self._add_to_lists()

    def __repr__(self):
        return f"({self.x},{self.y}) : {self.neighbours}"

    def _can_move(self):
        return self.neighbours != 0

    def calc_neighbours(self):
        self.neighbours = 0
        row_above = Elf.y.get(self.y - 1, {})
        row_on = Elf.y.get(self.y , {})
        row_below = Elf.y.get(self.y + 1, {})
        if row_above.get(self.x - 1, None):
            self.neighbours |= 0b1
        if row_above.get(self.x, None):
            self.neighbours |= 0b10
        if row_above.get(self.x + 1, None):
            self.neighbours |= 0b100
        if row_on.get(self.x - 1, None):
            self.neighbours |= 0b1000
        if row_on.get(self.x + 1, None):
            self.neighbours |= 0b10000
        if row_below.get(self.x - 1, None):
            self.neighbours |= 0b100000
        if row_below.get(self.x, None):
            self.neighbours |= 0b1000000
        if row_below.get(self.x + 1, None):
            self.neighbours |= 0b10000000

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for iy in range(0, len(data)):
        for ix in range(0, len(data[iy])):
            if data[iy][ix] == "#":
                Elf(ix, iy)

    start_mode = 0
    modes = {
        0: (0b00000111, lambda x,y : (x, y-1)),
        1: (0b11100000, lambda x,y : (x, y+1)),
        2: (0b00101001, lambda x,y : (x-1, y)),
        3: (0b10010100, lambda x,y : (x+1, y)),
    }

    move_count = 0
    while move_count < 10:
        move_count += 1
        proposed_moves = {}
        Elf.for_all(lambda elf: elf.calc_neighbours())

        for elf in Elf.can_move():
            for mode in range(start_mode, start_mode + 4, 1):
                test_mode = modes[mode % 4]
                if elf.neighbours & test_mode[0] == 0:
                    proposed_move = test_mode[1](elf.x, elf.y)
                    if proposed_move not in proposed_moves:
                        proposed_moves[proposed_move] = [elf]
                    else:
                        proposed_moves[proposed_move].append(elf)
                    break
        
        if len(proposed_moves) == 0:
            break
        
        for co_ordinates, elves in proposed_moves.items():
            if len(elves) == 1:
                elves[0].move_to(co_ordinates[0], co_ordinates[1])

        start_mode = (start_mode + 1) % 4
        x_keys_to_remove = [k for k in Elf.x.keys() if len(Elf.x[k]) == 0]
        for k in x_keys_to_remove:
            Elf.x.pop(k)
        y_keys_to_remove = [k for k in Elf.y.keys() if len(Elf.y[k]) == 0]
        for k in y_keys_to_remove:
            Elf.y.pop(k)

    width = 1+ max(Elf.x.keys()) - min(Elf.x.keys())
    height = 1+ max(Elf.y.keys()) - min(Elf.y.keys())
    answer = (width * height) - Elf.count()

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
