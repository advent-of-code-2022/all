# Day 22

OK so this one needs a bit of a README for Part 2...

I'm full of a cold/cough, the kids are off school being noisy, and I haven't got the brain to figure out how to automatically stitch together the faces of the cube.

So, there is a directory "nets", and in that are some config files.

The config files are named according to the "net type" - which is number of rows of faces in the net, divided by the number of columns in the net, all multiplied by 60 to keep it to a nice round number.  Pretty random, but it works :shrug:

All nets of squares are either 3x4, 4x3, 2x5, or 5x2.

So:
* the 5x2s are in file 24 (2/5 = 0.4, 0.4*60 = 24)
* the 2x5s in file 150
* the 4x3s in file 45
* and the 3x4s in file 80.

I've only built the config for the sample (which is a 4x3 layout) and my input (which is a 3x4 layout).  If you have a different layout type, you'll need to include it in the appropriate config file.  There's a relatively small finite number of possible layouts, but I don't have time or brain right now to figure them all out!

The config format is as follows:

```
# Any initial comments line starting with #
0010
0234
5600

mFnGRD:mFnGRD:.....


# Next config below after two blank lines...
```

* The first section is the positions of the faces in the net, numbered from 1 to 6, top left to bottom right, padded with zeroes, one row per row, one number per column.  Obviously the number of rows and columns will vary from one file to the next, but should be consistent within the same file.
* The second section is separated from the first by a blank line, and describes how the faces are stitched together, as a list of colon `:` separated instructions as follows:
    * `m` and `n` are face numbers to stitch together - in the example above we could stitch together faces 1 and 4
    * `F` describes which edge of face `m` to connect, and `G` describes which edge of face `n` to connect it to.  Connections are automatically reciprocal.  Edge names are:
        * "`U`" for Up, 
        * "`D`" for Down, 
        * "`L`" for Left, 
        * and "`R`" for Right.
    * In the example of connecting faces 1 and 4, `F` would be `R` for 1, and `G` would be `U` for 4.  So the first part of the instruction would be `1R4U`
    * The next character `R` describes whether or not we're reversing the numerical order of connections.  As the map lies flat, lower numbers are to the top and left.  If we want to keep the same order for both edges, `R` should be set to `=`.  If we want to reverse the order for one edge (i.e. as the value on one edge increases, the corresponding value on the other edge decreases), pass in `X` here.  For our 1 and 4 example, we DO want to reverse the order - the lowest number on 1 would connect to the highest number on 4, and vice versa.  So our instruction would now be `1R4UX`
    * The final character describes the direction change when we're going from `m` to `n`, or in our example from 1 to 4.  As we go off the Right edge and are coming in at the Top edge, we want to go from facing Right to facing Down, so a 90 degree turn to the right.  A turn to the right is represented by `+`; a turn to the left by `-`; and a reversal by `X`.  If no direction change is needed (e.g. if you were going from 2 to 3 in the example above), then `=`.
    * So our final instruction for our example is `1R4UX+`

A valid instruction set should mention EACH edge of EACH face ONCE ONLY - so each face should be included in a total of four instructions, either as the "from" or the "to" - it doesn't matter which, with one appearance for each of "`U`", "`D`", "`L`" and "`R`".