#!/usr/bin/env python3

import os

class Face:
    faces = {}

    def stitch(dimension, face_ix1, face_ix2, edge1, edge2, reverse, redirect):
        face_1_tiles = None
        face_1_setter = None
        if edge1 == "U":
            face_1_tiles = {ix: Face.faces[face_ix1].tiles[0].get(ix, None) for ix in range(0, dimension)}
            face_1_setter = Tile.assign_above
        elif edge1 == "D":
            face_1_tiles = {ix: Face.faces[face_ix1].tiles[dimension-1].get(ix, None) for ix in range(0, dimension)}
            face_1_setter = Tile.assign_below
        elif edge1 == "R":
            face_1_tiles = {ix: Face.faces[face_ix1].tiles[ix].get(dimension-1, None) for ix in range(0, dimension)}
            face_1_setter = Tile.assign_right
        elif edge1 == "L":
            face_1_tiles = {ix: Face.faces[face_ix1].tiles[ix].get(0, None) for ix in range(0, dimension)}
            face_1_setter = Tile.assign_left
        face_2_tiles = None
        face_2_setter = None
        if edge2 == "U":
            face_2_tiles = {ix: Face.faces[face_ix2].tiles[0].get(ix, None) for ix in range(0, dimension)}
            face_2_setter = Tile.assign_above
        elif edge2 == "D":
            face_2_tiles = {ix: Face.faces[face_ix2].tiles[dimension-1].get(ix, None) for ix in range(0, dimension)}
            face_2_setter = Tile.assign_below
        elif edge2 == "R":
            face_2_tiles = {ix: Face.faces[face_ix2].tiles[ix].get(dimension-1, None) for ix in range(0, dimension)}
            face_2_setter = Tile.assign_right
        elif edge2 == "L":
            face_2_tiles = {ix: Face.faces[face_ix2].tiles[ix].get(0, None) for ix in range(0, dimension)}
            face_2_setter = Tile.assign_left
        for ix in range(0, dimension):
            if face_1_tiles[ix] and face_2_tiles[ix] and not reverse:
                face_1_setter(face_1_tiles[ix], face_2_tiles[ix], redirect)
                face_2_setter(face_2_tiles[ix], face_1_tiles[ix], f"I{redirect}")
            if face_1_tiles[ix] and face_2_tiles[dimension-ix-1] and reverse:
                face_1_setter(face_1_tiles[ix], face_2_tiles[dimension-ix-1], redirect)
                face_2_setter(face_2_tiles[dimension-ix-1], face_1_tiles[ix], f"I{redirect}")

    def __init__(self, id, dimension, x_offset, y_offset, data):
        self.id = id
        self.dimension = dimension
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.tiles = {}
        for iy in range(0, len(data)):
            self.tiles[iy] = {}
            for ix in range(0, len(data[iy])):
                if data[iy][ix] == ".":
                    self.tiles[iy][ix] = Tile(iy + y_offset, ix + x_offset)
        for iy in range(0, len(data)):
            for ix in range(0, len(data[iy])-1):
                if ix in self.tiles[iy] and (ix+1) in self.tiles[iy]:
                    self.tiles[iy][ix].right = self.tiles[iy][ix+1]
                    self.tiles[iy][ix+1].left = self.tiles[iy][ix]
        for ix in range(0, len(data[0])):
            for iy in range(0, len(data[iy])-1):
                if ix in self.tiles[iy] and ix in self.tiles[iy+1]:
                    self.tiles[iy][ix].below = self.tiles[iy+1][ix]
                    self.tiles[iy+1][ix].above = self.tiles[iy][ix]
            Face.faces[id] = self

class Tile:
    selected_tile = None

    def assign_above(tile1, tile2, redirect):
        tile1.above = tile2
        if redirect.endswith("="):
            return
        elif redirect.endswith("X"):
            tile1.above_direction = "D"
        elif redirect.startswith("I"):
            if redirect.endswith("+"):
                tile1.above_direction = "L"
            elif redirect.endswith("-"):
                tile1.above_direction = "R"
        elif redirect.endswith("+"):
            tile1.above_direction = "R"
        elif redirect.endswith("-"):
            tile1.above_direction = "L"

    def assign_left(tile1, tile2, redirect):
        tile1.left = tile2
        if redirect.endswith("="):
            return
        elif redirect.endswith("X"):
            tile1.left_direction = "R"
        elif redirect.startswith("I"):
            if redirect.endswith("+"):
                tile1.left_direction = "D"
            elif redirect.endswith("-"):
                tile1.left_direction = "U"
        elif redirect.endswith("+"):
            tile1.left_direction = "U"
        elif redirect.endswith("-"):
            tile1.left_direction = "D"

    def assign_below(tile1, tile2, redirect):
        tile1.below = tile2
        if redirect.endswith("="):
            return
        elif redirect.endswith("X"):
            tile1.below_direction = "U"
        elif redirect.startswith("I"):
            if redirect.endswith("+"):
                tile1.below_direction = "R"
            elif redirect.endswith("-"):
                tile1.below_direction = "L"
        elif redirect.endswith("+"):
            tile1.below_direction = "L"
        elif redirect.endswith("-"):
            tile1.below_direction = "R"

    def assign_right(tile1, tile2, redirect):
        tile1.right = tile2
        if redirect.endswith("="):
            return
        elif redirect.endswith("X"):
            tile1.right_direction = "L"
        elif redirect.startswith("I"):
            if redirect.endswith("+"):
                tile1.right_direction = "U"
            elif redirect.endswith("-"):
                tile1.right_direction = "D"
        elif redirect.endswith("+"):
            tile1.right_direction = "D"
        elif redirect.endswith("-"):
            tile1.right_direction = "U"

    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.above = None
        self.above_direction = "U"
        self.below = None
        self.below_direction = "D"
        self.left = None
        self.left_direction = "L"
        self.right = None
        self.right_direction = "R"
        self.last_direction = None

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n\n")
    
    board = data[0].split("\n")
    total_rows = len(board)
    total_cols = max([len(row) for row in board])
    net_type = ((60 * total_rows) // total_cols)

    net_types = {
        24: (5,2,),
        150: (2,5,),
        80: (3,4,),
        45: (4,3,)
    }

    net_side_dimension = total_rows // net_types[net_type][1]
    net_sides = []

    net_side_ix = 1
    for iy in range(0, len(board), net_side_dimension):
        net_side = []
        row = board[iy] + "".join([" " * (total_cols - len(board[iy]))])
        left_gaps = (len(row) - len(row.lstrip(" "))) // net_side_dimension
        right_gaps = (len(row) - len(row.rstrip(" "))) // net_side_dimension
        for nx in range(0, total_cols // net_side_dimension):
            if nx < left_gaps:
                net_side.append(0)
            elif nx < ((total_cols // net_side_dimension) - right_gaps):
                net_side.append(net_side_ix)
                net_side_ix += 1
            else:
                net_side.append(0)
        net_sides.append(net_side)

    for iy in range(0, len(net_sides)):
        for ix in range(0, len(net_sides[iy])):
            if net_sides[iy][ix] != 0:
                face_data = [line[ix * net_side_dimension:(ix +1)* net_side_dimension] for line in board[iy * net_side_dimension:(iy+1)* net_side_dimension]]
                Face(net_sides[iy][ix], net_side_dimension, ix * net_side_dimension, iy * net_side_dimension, face_data)
    
    net_path = os.path.join(os.path.dirname(input_file),"nets/",str(net_type))
    with open(net_path, "r") as net_data:
        net_data = net_data.read().split("\n\n\n")

    for config in net_data:
        net_data_parts=config.split("\n\n")
        net_patterns=[list(map(int, v)) for v in net_data_parts[0].split("\n") if not v.startswith("#")]
        if net_patterns == net_sides:
            stitch_instructions = net_data_parts[1].split(":")
            for instruction in stitch_instructions:
                face_from = int(instruction[0])
                face_to = int(instruction[2])
                edge_from = instruction[1]
                edge_to = instruction[3]
                reverse = instruction[4] == "X"
                redirect = instruction[5]
                Face.stitch(net_side_dimension, face_from, face_to, edge_from, edge_to, reverse, redirect)
            break

    moves = data[1]
    moves = moves.replace("R", ":R:").replace("L",":L:").split(":")

    top_row_first_face = Face.faces[1].tiles[0]
    Tile.selected_tile = top_row_first_face[min(top_row_first_face.keys())]
    direction = "R"

    directions = {
        "U": ("L","R", lambda: (Tile.selected_tile.above, Tile.selected_tile.above_direction) if Tile.selected_tile.above else (Tile.selected_tile, direction), 3, "^"),
        "R": ("U","D", lambda: (Tile.selected_tile.right, Tile.selected_tile.right_direction) if Tile.selected_tile.right else (Tile.selected_tile, direction), 0, ">"),
        "D": ("R","L", lambda: (Tile.selected_tile.below, Tile.selected_tile.below_direction) if Tile.selected_tile.below else (Tile.selected_tile, direction), 1, "V"),
        "L": ("D","U", lambda: (Tile.selected_tile.left, Tile.selected_tile.left_direction) if Tile.selected_tile.left else (Tile.selected_tile, direction), 2, "<"),
    }

    for move in moves:
        if move == "L":
            direction = directions[direction][0]
            Tile.selected_tile.last_direction = directions[direction][4]
        elif move == "R":
            direction = directions[direction][1]
            Tile.selected_tile.last_direction = directions[direction][4]
        else:
            count = int(move)
            while count > 0:
                count -= 1
                prev_tile = Tile.selected_tile
                Tile.selected_tile, direction = directions[direction][2]()
                Tile.selected_tile.last_direction = directions[direction][4]
                if Tile.selected_tile == prev_tile:
                    break

    answer = ((Tile.selected_tile.row+1) * 1000) + ((Tile.selected_tile.col+1) * 4) + directions[direction][3]
    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
