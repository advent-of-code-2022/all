#!/usr/bin/env python3

import os

class Air:
    air_xy = {}
    air_xz = {}
    air_yz = {}
    cube_faces = 0
    walk_list = []

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        if (x,y) not in Air.air_xy:
            Air.air_xy[(x,y)] = {z: self}
        else:
            Air.air_xy[(x,y)][z] = self
        if (x,z) not in Air.air_xz:
            Air.air_xz[(x,z)] = {y: self}
        else:
            Air.air_xz[(x,z)][y] = self
        if (y,z) not in Air.air_yz:
            Air.air_yz[(y,z)] = {x: self}
        else:
            Air.air_yz[(y,z)][x] = self

    def walk(initial, x_bounds, y_bounds, z_bounds):
        Air.walk_list.extend(initial)
        while len(Air.walk_list) > 0:
            item = Air.walk_list.pop()
            if item.x - 1 >= x_bounds[0]:
                if (item.x - 1) not in Air.air_yz[(item.y, item.z)]:
                    if ((item.y, item.z) not in Cube.cubes_yz) or (item.x - 1) not in Cube.cubes_yz[(item.y, item.z)]:
                        Air.walk_list.append(Air(item.x - 1, item.y, item.z))
                    else:
                        Air.cube_faces += 1
            if item.x + 1 <= x_bounds[1]:
                if (item.x + 1) not in Air.air_yz[(item.y, item.z)]:
                    if ((item.y, item.z) not in Cube.cubes_yz) or (item.x + 1) not in Cube.cubes_yz[(item.y, item.z)]:
                        Air.walk_list.append(Air(item.x + 1, item.y, item.z))
                    else:
                        Air.cube_faces += 1
            if item.y - 1 >= y_bounds[0]:
                if (item.y - 1) not in Air.air_xz[(item.x, item.z)]:
                    if ((item.x, item.z) not in Cube.cubes_xz) or (item.y - 1) not in Cube.cubes_xz[(item.x, item.z)]:
                        Air.walk_list.append(Air(item.x, item.y - 1, item.z))
                    else:
                        Air.cube_faces += 1
            if item.y + 1 <= y_bounds[1]:
                if (item.y + 1) not in Air.air_xz[(item.x, item.z)]:
                    if ((item.x, item.z) not in Cube.cubes_xz) or (item.y + 1) not in Cube.cubes_xz[(item.x, item.z)]:
                        Air.walk_list.append(Air(item.x, item.y + 1, item.z))
                    else:
                        Air.cube_faces += 1
            if item.z - 1 >= z_bounds[0]:
                if (item.z - 1) not in Air.air_xy[(item.x, item.y)]:
                    if ((item.x, item.y) not in Cube.cubes_xy) or (item.z - 1) not in Cube.cubes_xy[(item.x, item.y)]:
                        Air.walk_list.append(Air(item.x, item.y, item.z - 1))
                    else:
                        Air.cube_faces += 1
            if item.z + 1 <= z_bounds[1]:
                if (item.z + 1) not in Air.air_xy[(item.x, item.y)]:
                    if ((item.x, item.y) not in Cube.cubes_xy) or (item.z + 1) not in Cube.cubes_xy[(item.x, item.y)]:
                        Air.walk_list.append(Air(item.x, item.y, item.z + 1))
                    else:
                        Air.cube_faces += 1

class Cube:
    cubes_xy = {}
    cubes_xz = {}
    cubes_yz = {}

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        if (x,y) not in Cube.cubes_xy:
            Cube.cubes_xy[(x,y)] = {z: self}
        else:
            Cube.cubes_xy[(x,y)][z] = self
        if (x,z) not in Cube.cubes_xz:
            Cube.cubes_xz[(x,z)] = {y: self}
        else:
            Cube.cubes_xz[(x,z)][y] = self
        if (y,z) not in Cube.cubes_yz:
            Cube.cubes_yz[(y,z)] = {x: self}
        else:
            Cube.cubes_yz[(y,z)][x] = self

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for line in data:
        Cube(*[int(v) for v in line.split(",")])

    z_values = {k for d in Cube.cubes_xy.values() for k in d.keys()}
    y_values = {k for d in Cube.cubes_xz.values() for k in d.keys()}
    x_values = {k for d in Cube.cubes_yz.values() for k in d.keys()}
    z_bounds = (min(z_values) - 2, max(z_values) + 2)
    y_bounds = (min(y_values) - 2, max(y_values) + 2)
    x_bounds = (min(x_values) - 2, max(x_values) + 2)

    starting_air = Air(x_bounds[0], y_bounds[0], z_bounds[0])
    Air.walk([starting_air],x_bounds, y_bounds, z_bounds)
    answer = Air.cube_faces

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
