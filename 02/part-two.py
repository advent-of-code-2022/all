#!/usr/bin/env python3

import os

def answer(input_file):
    score_for_result = {
        #               THEM    OUTCOME   WE PLAY           SHAPE POINTS           RESULT POINTS
        "A X": 3 + 0, # Rock     - Loss : Scissors to Lose, 3 points for Scissors, 0 points for Loss
        "A Y": 1 + 3, # Rock     - Draw : Rock to Draw,     1 point for Rock,      3 points for Draw
        "A Z": 2 + 6, # Rock     - Win  : Paper to Win,     2 points for Paper,    6 points for Win
        "B X": 1 + 0, # Paper    - Loss : Rock to Lose,     1 point for Rock,      0 points for Loss
        "B Y": 2 + 3, # Paper    - Draw : Paper to Draw,    2 points for Paper,    3 points for Draw
        "B Z": 3 + 6, # Paper    - Win  : Scissors to Win,  3 points for Scissors, 6 points for Win
        "C X": 2 + 0, # Scissors - Loss : Paper to Lose,    2 points for Paper,    0 points for Loss
        "C Y": 3 + 3, # Scissors - Draw : Scissors to Draw, 3 points for Scissors, 3 points for Draw
        "C Z": 1 + 6, # Scissors - Win  : Rock to Win,      1 points for Rock,     6 points for Win
    }

    score = 0
    with open(input_file, "r") as input:
        data = [line.strip() for line in input.readlines()]

    for line in data:
        score += score_for_result[line]

    print(f"Answer: Score is *** {score} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
