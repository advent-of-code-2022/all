#!/usr/bin/env python3

import os

def answer(input_file):
    score_for_result = {
        #               THEM       US         SHAPE POINTS            RESULT POINTS
        "A X": 1 + 3, # Rock     - Rock     - 1 point for Rock        3 points for a draw
        "A Y": 2 + 6, # Rock     - Paper    - 2 points for Paper      6 points for a win
        "A Z": 3 + 0, # Rock     - Scissors - 3 points for Scissors   0 points for a loss
        "B X": 1 + 0, # Paper    - Rock     - 1 point for Rock        0 points for a loss
        "B Y": 2 + 3, # Paper    - Paper    - 2 points for Paper      3 points for a draw
        "B Z": 3 + 6, # Paper    - Scissors - 3 points for Scissors   6 points for a win
        "C X": 1 + 6, # Scissors - Rock     - 1 point for Rock        6 points for a win
        "C Y": 2 + 0, # Scissors - Paper    - 2 points for Paper      0 points for a loss
        "C Z": 3 + 3, # Scissors - Scissors - 3 points for Scissors   3 points for a draw
    }

    score = 0
    with open(input_file, "r") as input:
        data = [line.strip() for line in input.readlines()]

    for line in data:
        score += score_for_result[line]

    print(f"Answer: Score is *** {score} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
