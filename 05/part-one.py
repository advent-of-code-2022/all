#!/usr/bin/env python3

import os

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n\n")

    stack_data = data[0].split("\n")[::-1]
    stack_moves = data[1].split("\n")
    stack_count = (len(stack_data[0]) + 1) // 4
    stack_data = stack_data[1:]
    stacks = dict.fromkeys(range(0, stack_count))
    for ix in stacks.keys():
        stacks[ix] = []
    
    for line in stack_data:
        stack_line_data = [line[ix+1] for ix in range(0, len(line), 4)]
        for ix in stacks.keys():
            if stack_line_data[ix] != " ":
                stacks[ix].append(stack_line_data[ix])
    
    for move in stack_moves:
        command = move.split(" ")
        num_crates = int(command[1])
        from_stack = int(command[3]) - 1
        to_stack = int(command[5]) - 1
        stacks[to_stack] += stacks[from_stack][-1:-(num_crates+1):-1]
        stacks[from_stack] = stacks[from_stack][:-num_crates]
    
    answer = "".join(map(lambda x: x[-1], stacks.values()))
    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
