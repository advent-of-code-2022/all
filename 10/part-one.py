#!/usr/bin/env python3

import os

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    register_X = 1
    cycle_count = 1
    signal_strengths = {}
    for line in data:
        if line[0:4] == "addx":
            cycle_count += 1
            if cycle_count % 40 == 20:
                signal_strengths[cycle_count] = cycle_count * register_X
            cycle_count +=1
            register_X += int(line[5:])
        else:
            cycle_count += 1

        if cycle_count % 40 == 20:
            signal_strengths[cycle_count] = cycle_count * register_X

    answer = sum(signal_strengths.values())

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
