#!/usr/bin/env python3

import os

def hash_dots_to_blocks(input):
    reset = "\033[0;0m"
    white = "\033[0;30;47m"
    return input.replace("\n",f"{reset}\n").replace(".", f"{reset} ").replace("#", f"{white} ") + reset

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    crt_data = []

    def crt_cycle():
        nonlocal cycle_count, crt_data, register_X
        if cycle_count - (40 * (cycle_count // 40)) in (register_X - 1, register_X, register_X + 1):
            crt_data.append("#")
        else:
            crt_data.append(".")
        cycle_count += 1

    register_X = 1
    cycle_count = 0
    for line in data:
        if line[0:4] == "addx":
            crt_cycle()
            crt_cycle()
            register_X += int(line[5:])
        else:
            crt_cycle()

    crt_display = "".join(crt_data)
    print(hash_dots_to_blocks("\n".join([crt_display[ix:ix+40] for ix in range(0, 240, 40)])))
 
input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
