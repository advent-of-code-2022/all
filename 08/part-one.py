#!/usr/bin/env python3

import os

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = [[int(tree) for tree in line] for line in input_data.read().split("\n")]

    width, height = len(data[0]), len(data)

    visible = [[None for _ in range(width)] for _ in range(height)]

    # From left:
    for iy in range(height):
        max_from_left = -1
        for ix in range(width):
            if data[iy][ix] > max_from_left:
                visible[iy][ix] = True
                max_from_left = data[iy][ix]

    # From right:
    for iy in range(height):
        max_from_right = -1
        for ix in range(width - 1, -1, -1):
            if data[iy][ix] > max_from_right:
                visible[iy][ix] = True
                max_from_right = data[iy][ix]

    # From top:
    for ix in range(width):
        max_from_top = -1
        for iy in range(height):
            if data[iy][ix] > max_from_top:
                visible[iy][ix] = True
                max_from_top = data[iy][ix]
            
    # From bottom:
    for ix in range(width):
        max_from_bottom = -1
        for iy in range(height - 1, -1, -1):
            if data[iy][ix] > max_from_bottom:
                visible[iy][ix] = True
                max_from_bottom = data[iy][ix]
            
    answer = sum([sum([tree for tree in row if tree]) for row in visible])

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
