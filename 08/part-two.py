#!/usr/bin/env python3

import os

def calculate_score(x, y, data):
    width, height = len(data[0]), len(data)
    tree_value = data[y][x]

    # look up
    up_score = 0
    for iy in range (y - 1, -1, -1):
        up_score += 1
        if data[iy][x] >= tree_value:
            break
    
    # look down
    down_score = 0
    for iy in range (y + 1, height):
        down_score += 1
        if data[iy][x] >= tree_value:
            break

    # look left
    left_score = 0
    for ix in range (x - 1, -1, -1):
        left_score += 1
        if data[y][ix] >= tree_value:
            break

    # look right
    right_score = 0
    for ix in range (x + 1, width):
        right_score += 1
        if data[y][ix] >= tree_value:
            break
    
    return up_score * down_score * left_score * right_score

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = [[int(tree) for tree in line] for line in input_data.read().split("\n")]

    width, height = len(data[0]), len(data)

    answer = 0
    for iy in range(height):
        for ix in range(width):
            answer = max(answer, calculate_score(ix, iy, data))

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
