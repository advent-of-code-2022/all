#!/usr/bin/env python3

import os

class Valve:

    valves = {}

    def for_all(function):
        [function(valve) for valve in Valve.valves.values()]

    def useful_valves():
        return {valve.name for valve in Valve.valves.values() if valve.flow > 0}

    def __init__(self, data):
        items = data.split(" ")
        self.name = items[1]
        self.flow = int(items[4][5:-1])
        self.neighbour_names = [item.strip(",") for item in items[9:]]
        self.distances = {
            k: 1 for k in [item.strip(",") for item in items[9:]]
        }
        Valve.valves[self.name] = self
    
    def hook_up_neighbours(self):
        self.neighbours = {
            name: Valve.valves[name] for name in self.neighbour_names
        }

    def calc_distance_to(self, target_name, from_names = []):
        if target_name == self.name:
            return 0
        if target_name in self.distances:
            return self.distances[target_name]
        neighbour_distances = [neighbour.calc_distance_to(target_name, from_names + [self.name]) for name, neighbour in self.neighbours.items() if name not in from_names]
        neighbour_distances = [neighbour for neighbour in neighbour_distances if neighbour is not None]
        if len(neighbour_distances) == 0:
            return None
        self.distances[target_name] = min(neighbour_distances) + 1
        return self.distances[target_name]

class State:
    def __init__(self, step, location, valves_open, flow_rate, cumulative_flow):
        self.step = step
        self.location = location
        self.valves_open = valves_open
        self.flow_rate = flow_rate
        self.cumulative_flow = cumulative_flow
    
    def __repr__(self):
        return f"@{self.step} {self.cumulative_flow} {self.flow_rate} {self.location} {self.valves_open}"

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")
    
    for line in data:
        Valve(line)

    Valve.for_all(lambda valve: valve.hook_up_neighbours())
    for valve in Valve.valves.values():
        for name in Valve.valves.keys():
            valve.calc_distance_to(name)
    
    initial = State(0, "AA", set(), 0, 0)
    ix = 0
    states_to_process = [initial]
    useful_valve_names = Valve.useful_valves()
    max_flow = 0
    while ix < len(states_to_process):
        process = states_to_process[ix]
        ix += 1
        if process.cumulative_flow > max_flow:
            max_flow = process.cumulative_flow
        if process.step == 30:
            continue
        current_valve = Valve.valves[process.location]
        if process.location not in process.valves_open and process.location in useful_valve_names:
            # Open valve
            open_valves = set.union({valve for valve in process.valves_open}, {process.location})
            states_to_process.append(State(process.step + 1, process.location, open_valves, process.flow_rate + current_valve.flow, process.cumulative_flow + process.flow_rate))
        else:
            unopened_useful_valves = useful_valve_names - process.valves_open
            distances = {
                valve_name: current_valve.distances[valve_name] for valve_name in unopened_useful_valves
            }
            # Try moving to all unopened useful valves
            new_states_to_try = [
                State(process.step + distance, valve_name, process.valves_open, process.flow_rate, process.cumulative_flow + (process.flow_rate * distance))
                for valve_name, distance in distances.items() if process.step + distance <= 30
            ]
            if new_states_to_try:
                states_to_process.extend(new_states_to_try)
            else:
                # Stand still until 30 minutes are up :D
                steps = 30 - process.step
                states_to_process.append(State(process.step + steps, process.location, process.valves_open, process.flow_rate, process.cumulative_flow + (process.flow_rate * steps)))
        
    answer = max_flow

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
