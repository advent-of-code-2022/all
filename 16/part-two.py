#!/usr/bin/env python3

import os

class Valve:

    valves = {}
    valve_masks = {}
    valve_names = []

    def for_all(function):
        [function(valve) for valve in Valve.valves.values()]

    def useful_valves():
        mask = 0
        for valve in Valve.valves.values():
            if valve.flow > 0:
                mask |= valve.mask
        return mask

    def __init__(self, data):
        items = data.split(" ")
        self.name = items[1]
        Valve.valve_names.append(self.name)
        ix = Valve.valve_names.index(self.name)
        self.mask = 1 << ix
        self.flow = int(items[4][5:-1])
        self.neighbour_names = [item.strip(",") for item in items[9:]]
        Valve.valves[self.name] = self
        Valve.valve_masks[self.mask] = self
    
    def hook_up_neighbours(self):
        self.neighbours = {
            Valve.valves[name].mask: Valve.valves[name] for name in self.neighbour_names
        }
        self.distances = {
            Valve.valves[name].mask: 1 for name in self.neighbour_names
        }

    def calc_distance_to(self, target_mask, from_mask = 0):
        if target_mask == self.mask:
            return 0
        if target_mask in self.distances:
            return self.distances[target_mask]
        neighbour_distances = [neighbour.calc_distance_to(target_mask, from_mask | self.mask) for mask, neighbour in self.neighbours.items() if mask & from_mask == 0]
        neighbour_distances = [neighbour for neighbour in neighbour_distances if neighbour is not None]
        if len(neighbour_distances) == 0:
            return None
        self.distances[target_mask] = min(neighbour_distances) + 1
        return self.distances[target_mask]

class State:
    def __init__(self, parent, step, location, valves_open, flow_rate, cumulative_flow):
        self.parent = parent
        self.step = step
        self.location = location
        self.valves_open = valves_open
        self.flow_rate = flow_rate
        self.cumulative_flow = cumulative_flow
    
    def __repr__(self):
        return f"{self.parent}\n@{self.step} {self.cumulative_flow} {self.flow_rate} {bin(self.location)} {bin(self.valves_open)}"

def answer(input_file):
    def process_state(state: State):
        nonlocal states_to_process, best_for_each_valve_combination
        record_best_values(state)
        if state.step == 26:
            return
        current_valve = Valve.valve_masks[state.location]
        open_valves = state.valves_open
        flow = state.flow_rate
        if state.location & useful_valves and not state.location & open_valves:
            open_valves |= state.location
            flow += current_valve.flow
        remaining_useful_valves = useful_valves & ~open_valves
        distances = {
            mask: current_valve.distances[mask] for mask in Valve.valve_masks.keys() if mask & remaining_useful_valves and not mask & state.location
        }

        new_states_to_try = [
            State(state, state.step + distance + 1, valve_mask, open_valves, flow, state.flow_rate + state.cumulative_flow + (flow* distance))
            for valve_mask, distance in distances.items() if state.step + distance + 1 <= 26
        ]
        states_to_process.extend(new_states_to_try)
        
        steps = 26 - state.step
        states_to_process.append(State(state, state.step + steps, state.location, open_valves, flow, state.flow_rate + state.cumulative_flow + (flow * steps)))

    def record_best_values(state):
        nonlocal best_for_each_valve_combination
        if state.valves_open and state.valves_open not in best_for_each_valve_combination:
            best_for_each_valve_combination[state.valves_open] = state
        elif state.valves_open and best_for_each_valve_combination[state.valves_open].cumulative_flow <= state.cumulative_flow:
            best_for_each_valve_combination[state.valves_open] = state

    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")
    
    for line in data:
        Valve(line)

    Valve.for_all(lambda valve: valve.hook_up_neighbours())
    for valve in Valve.valves.values():
        for mask in Valve.valve_masks.keys():
            valve.calc_distance_to(mask)
    
    initial = State(None, 0, Valve.valves["AA"].mask, 0, 0, 0)
    states_to_process = [initial]
    useful_valves = Valve.useful_valves()
    best_for_each_valve_combination = {}
    while len(states_to_process) > 0:
        state = states_to_process.pop()
        process_state(state)

    answer = 0
    for k1, v1 in best_for_each_valve_combination.items():
        for k2, v2 in best_for_each_valve_combination.items():
            if not k1 & k2:
                if v1.cumulative_flow + v2.cumulative_flow > answer:
                    answer = v1.cumulative_flow + v2.cumulative_flow

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
