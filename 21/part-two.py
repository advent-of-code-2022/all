#!/usr/bin/env python3

import os, operator

class Instruction:

    instructions = {}

    calc_queue = []

    def get_dependencies(instruction):
        result = set()
        next_instructions = [instruction]
        while len(next_instructions) > 0:
            instruction = next_instructions.pop()
            if instruction.op1_identifier is not None:
                if instruction.op1_identifier not in result:
                    result.add(instruction.op1_identifier)
                    next_instructions.append(instruction.op1)
            if instruction.op2_identifier is not None:
                if instruction.op2_identifier not in result:
                    result.add(instruction.op2_identifier)
                    next_instructions.append(instruction.op2)
        return result

    operators = {
        "+": operator.add,
        "-": operator.sub,
        "/": operator.floordiv,
        "*": operator.mul
    }

    def __init__(self, line):
        parts = line.split(": ")
        self.identifier = parts[0]
        operations = parts[1].strip(" ").split(" ")
        if len(operations) == 1:
            self.value = int(operations[0])
            self.op1_identifier = None
            self.op2_identifier = None
            self.op1 = None
            self.op2 = None
        else:
            self.op1_identifier = operations[0]
            self.op2_identifier = operations[2]
            self.op1 = None
            self.op2 = None
            self.op = Instruction.operators[operations[1]]
            self.value = None
        Instruction.instructions[self.identifier] = self

    def hook_up_dependencies(self):
        if self.value:
            return
        if not self.op1:
             self.op1 = Instruction.instructions[self.op1_identifier]
        if not self.op2:
             self.op2 = Instruction.instructions[self.op2_identifier]

    def calc(self):
        if self.value:
            return self.value
        if not self.op1:
            self.op1 = Instruction.instructions[self.op1_identifier]
        if not self.op2:
            self.op2 = Instruction.instructions[self.op2_identifier]
        if self.op1.value is not None and self.op2.value is not None:
            self.value = self.op(self.op1.value, self.op2.value)
            return self.value
        if self.value is None:
            Instruction.calc_queue.append(self)
        if self.op1.value is None:
            Instruction.calc_queue.append(self.op1)
        if self.op2.value is None:
            Instruction.calc_queue.append(self.op2)
        return None

    def calc_backwards(self):
        if self.value and not self.op1 and not self.op2:
            return None
        if self.op1.value is None and self.op2.value is None:
            if self.op1_identifier == "humn":
                Instruction.calc_queue = [self.op2]
                while len(Instruction.calc_queue) > 0:
                    Instruction.calc_queue.pop().calc()
            elif self.op2_identifier == "humn":
                Instruction.calc_queue = [self.op1]
                while len(Instruction.calc_queue) > 0:
                    Instruction.calc_queue.pop().calc()
            else:
                raise Exception("Cannot calculate backwards, both operands are unknown")
        if self.op1.value is not None and self.op2.value is not None:
            raise Exception("Cannot calculate backwards, both operands are already known")
        if self.op1.value is not None:
            if self.op is Instruction.operators["+"]:
                self.op2.value = self.value - self.op1.value
            if self.op is Instruction.operators["-"]:
                self.op2.value = self.op1.value - self.value
            if self.op is Instruction.operators["*"]:
                self.op2.value = self.value // self.op1.value
            if self.op is Instruction.operators["/"]:
                self.op2.value = self.op1.value // self.value
            return self.op2
        if self.op2.value is not None:
            if self.op is Instruction.operators["+"]:
                self.op1.value = self.value - self.op2.value
            if self.op is Instruction.operators["-"]:
                self.op1.value = self.op2.value + self.value
            if self.op is Instruction.operators["*"]:
                self.op1.value = self.value // self.op2.value
            if self.op is Instruction.operators["/"]:
                self.op1.value = self.op2.value * self.value
            return self.op1

def calc_non_human_branch(start_from):
    start = Instruction.instructions[start_from]
    if start.op1_identifier is not None and start.op2_identifier is not None:
        dependencies = [Instruction.get_dependencies(start.op1), Instruction.get_dependencies(start.op2)]
    else:
        return None, start.value
    human_tree = ([dep for dep in dependencies if "humn" in dep] or [None])[0]
    if not human_tree:
        return None
    if dependencies.index(human_tree) == 0:
        known_root = start.op2
        human_root = start.op1
    else:
        known_root = start.op1
        human_root = start.op2

    known_val = None
    while known_val is None:
        known_val = known_root.calc()
        if known_val is None:
            while len(Instruction.calc_queue) > 0:
                Instruction.calc_queue.pop().calc()
    
    return human_root.identifier

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for line in data:
        Instruction(line)

    for instruction in Instruction.instructions.values():
        instruction.hook_up_dependencies()

    Instruction.instructions["humn"].value = None

    start_from = "root"
    while start_from:
        start_from = calc_non_human_branch(start_from)

    root = Instruction.instructions["root"]
    if root.op1.value is None:
        calc_next = root.op1
        root.op1.value = root.op2.value
    else:
        calc_next = root.op2
        root.op2.value = root.op1.value

    while calc_next is not None:
        calc_next = calc_next.calc_backwards()

    answer = Instruction.instructions["humn"].value
    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
