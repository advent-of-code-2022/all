#!/usr/bin/env python3

import os, operator

class Instruction:

    instructions = {}

    calc_queue = []

    operators = {
        "+": operator.add,
        "-": operator.sub,
        "/": operator.floordiv,
        "*": operator.mul
    }

    def __init__(self, line):
        parts = line.split(": ")
        self.identifier = parts[0]
        operations = parts[1].strip(" ").split(" ")
        if len(operations) == 1:
            self.value = int(operations[0])
        else:
            self.op1_identifier = operations[0]
            self.op2_identifier = operations[2]
            self.op1 = None
            self.op2 = None
            self.op = Instruction.operators[operations[1]]
            self.value = None
        Instruction.instructions[self.identifier] = self
    
    def calc(self):
        if self.value:
            return self.value
        if not self.op1:
            self.op1 = Instruction.instructions[self.op1_identifier]
        if not self.op2:
            self.op2 = Instruction.instructions[self.op2_identifier]
        if self.op1.value is not None and self.op2.value is not None:
            self.value = self.op(self.op1.value, self.op2.value)
            return self.value
        if self.value is None:
            Instruction.calc_queue.append(self)
        if self.op1.value is None:
            Instruction.calc_queue.append(self.op1)
        if self.op2.value is None:
            Instruction.calc_queue.append(self.op2)
        return None

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for line in data:
        Instruction(line)

    root = Instruction.instructions["root"]
    answer = root.calc()
    if answer is None:
        while len(Instruction.calc_queue) > 0:
            Instruction.calc_queue.pop().calc()
        answer = root.calc()

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
