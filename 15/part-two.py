#!/usr/bin/env python3

import os, sys

class Range:
    def __init__(self):
        self.exclusions = {}
    
    def exclude(self, start, end):
        if start not in self.exclusions:
            self.exclusions[start] = end
        else:
            ee = self.exclusions.get(start, -sys.maxsize)
            self.exclusions[start] = ee if ee > end else end

    def consolidate(self):
        consolidate = True
        start_values = sorted(self.exclusions.keys())
        range_end = len(start_values) - 1
        while consolidate:
            consolidate = False
            for ix in range(range_end):
                current_start = start_values[ix]
                next_start = start_values[ix+1]
                current_end = self.exclusions[current_start]
                if current_end >= next_start - 1:
                    next_end = self.exclusions[next_start]
                    if next_end > current_end:
                        self.exclusions[current_start] = next_end
                    del(self.exclusions[next_start])
                    del(start_values[ix+1])
                    range_end -= 1
                    consolidate = True
                    break
        return len(start_values)


class Beacon:
    def __init__(self, data):
        self.sx = int(data[2][2:-1])
        self.sy = int(data[3][2:-1])
        self.bx = int(data[8][2:-1])
        self.by = int(data[9][2:])
        self.distance = abs(self.sx - self.bx) + abs(self.sy - self.by)
        self.min_y = self.sy - self.distance
        self.max_y = self.sy + self.distance

    def get_row_blocked_indices(self, row, max_range):
        dist_remaining = self.distance - abs(row - self.sy)

        start_pos = self.sx - dist_remaining
        end_pos = self.sx + dist_remaining
        return start_pos if start_pos >= 0 else 0, end_pos if end_pos <= max_range else max_range

def answer(input_file, max_range):
    with open(input_file, "r") as input_data:
        data = [line.split(" ") for line in input_data.read().split("\n")]

    beacons = []
    for line in data:
        beacons.append(Beacon(line))

    progress = max_range // 80
    for ix in range(0, max_range):
        if ix % progress == 0:
            print('.', end='', flush=True)
        row_range = Range()
        for beacon in [beacon for beacon in beacons if ix >= beacon.min_y and ix <= beacon.max_y]:
            es, ee = beacon.get_row_blocked_indices(ix, max_range)
            row_range.exclude(es, ee)
        if row_range.consolidate() > 1:
            break
    
    print()
    answer = (max_range * (row_range.exclusions[0] + 1)) + ix
    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
max_range = 4000000
answer(input_file, max_range)
