#!/usr/bin/env python3

import os

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read()

    for ix in range(0, len(data)-4):
        if len(set(data[ix:ix+4])) == 4:
            answer = ix + 4
            break

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
