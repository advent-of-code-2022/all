#!/usr/bin/env python3

import os

class Cave:
    WIDTH_BITS=10
    HEIGHT_BITS=8
    WIDTH_BYTES=1 << WIDTH_BITS

    def __init__(self, data):
        x_offsets = [iy << Cave.HEIGHT_BITS for iy in range(1 << Cave.WIDTH_BITS)]
        walls = bytearray(1 << (Cave.WIDTH_BITS + Cave.HEIGHT_BITS))
        floor_level = 0
        for line in data:
            vertices = [(int(vertex.split(",")[0]),int(vertex.split(",")[1])) for vertex in line.split(" -> ")]
            for ia in range(len(vertices) - 1):
                sx, sy = vertices[ia]
                ex, ey = vertices[ia + 1]
                floor_level = max(floor_level, sy, ey)
                if sx == ex:
                    x_offset = x_offsets[sx]
                    for ib in range(min(sy,ey), max(sy,ey) + 1):
                        walls[x_offset + ib] = 0B1
                else:
                    for ib in range(min(sx,ex), max(sx,ex) + 1):
                        walls[(ib << Cave.HEIGHT_BITS) + sy] = 0B1
        floor_level += 2
        for ix in range(Cave.WIDTH_BYTES):
            walls[x_offsets[ix] + floor_level] = 0B1
        self._walls = walls
        self.floor_level = floor_level
        self.x_offsets = x_offsets
        self.sand_count = 0

    def get_wall_below(self, x, y):
        iy = self.x_offsets[x] + y
        while True:
            if self._walls[iy] != 0B0:
                return iy - self.x_offsets[x]
            iy += 1

    def pour_sand(self, px, py):
        while True:
            wall_below = self.get_wall_below(px, py)
            if wall_below == self.floor_level:
                self._walls[self.x_offsets[px] + wall_below - 1] = 0B10
                self.sand_count += 1
                px, py = 500, 0
            elif self._walls[self.x_offsets[px - 1] + wall_below] == 0B0:
                px -= 1
                py = wall_below
            elif self._walls[self.x_offsets[px + 1] + wall_below] == 0B0:
                px += 1
                py = wall_below
            else:
                self._walls[self.x_offsets[px] + wall_below - 1] = 0B10
                self.sand_count += 1
                if px == 500 and (wall_below - 1) == 0:
                    return None
                px, py = 500, 0

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    cave = Cave(data)
    cave.pour_sand(500,0)
    answer = cave.sand_count

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
