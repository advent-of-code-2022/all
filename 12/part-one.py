#!/usr/bin/env python3

import os, string, sys

class Point:
    data={}
    min_x = 0
    min_y = 0
    start_point = None
    end_point = None

    def setup(data):
        Point.max_x = len(data[0]) - 1
        Point.max_y = len(data) - 1
        for iy in range(len(data)):
            for ix in range(len(data[0])):
                Point(ix, iy, data[iy][ix])
        Point.for_all(lambda point: point.hook_up_neighbours())

    def for_all(function):
        [function(point) for sublist in Point.data.values() for point in sublist.values()]

    def walk():
        to_walk = [(-1,Point.start_point)]
        while len(to_walk) > 0:
            steps, point = to_walk.pop(0)
            if point.distance > steps + 1:
                point.distance = steps + 1
                to_walk.extend([(point.distance, neighbour) for neighbour in point.neighbours if neighbour.height <= point.height + 1])

    def __init__(self, x, y, init_value):
        if init_value == "S":
            Point.start_point = self
            self.height = 0
        elif init_value == "E":
            Point.end_point = self
            self.height = 25
        else:
            self.height = string.ascii_lowercase.index(init_value)
        self.x = x
        self.y = y
        self.distance = sys.maxsize
        self.neighbours = []
        if x not in Point.data.keys():
            Point.data[x] = {y: self}
        else:
            Point.data[x][y] = self

    def hook_up_neighbours(self):
        if self.x > Point.min_x:
            self.neighbours.append(Point.data[self.x - 1][self.y])
        if self.x < Point.max_x:
            self.neighbours.append(Point.data[self.x + 1][self.y])
        if self.y > Point.min_y:
            self.neighbours.append(Point.data[self.x][self.y - 1])
        if self.y < Point.max_y:
            self.neighbours.append(Point.data[self.x][self.y + 1])

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")
    
    Point.setup(data)
    Point.walk()
    answer = Point.end_point.distance

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
