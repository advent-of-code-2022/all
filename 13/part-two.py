#!/usr/bin/env python3

import os, json

def is_less_than(left, right):
    for ix in range(max(len(left), len(right))):
        if ix >= len(left):
            return True
        elif ix >= len(right):
            return False
        elif isinstance(left[ix], list) and isinstance(right[ix], list):
            res = is_less_than(left[ix], right[ix])
            if res is not None:
                return res
        elif isinstance(left[ix], int) and isinstance(right[ix], list):
            res = is_less_than([left[ix]], right[ix])
            if res is not None:
                return res
        elif isinstance(left[ix], list) and isinstance(right[ix], int):
            res = is_less_than(left[ix], [right[ix]])
            if res is not None:
                return res
        elif left[ix] < right[ix]:
            return True
        elif right[ix] < left[ix]:
                return False
    return None

def partition(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if is_less_than(array[j],pivot):
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1

def quicksort(array, low, high):
    if low < high:
        partition_index = partition(array, low, high)
        quicksort(array, low, partition_index - 1)
        quicksort(array, partition_index + 1, high)

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = [json.loads(line) for line in input_data.read().split("\n") if line]

    data.append([[2]])
    data.append([[6]])

    quicksort(data, 0, len(data) - 1)

    answer = (data.index([[2]]) + 1) * (data.index([[6]]) + 1) 

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
