#!/usr/bin/env python3

import os, json

def is_less_than(left, right):
    for ix in range(max(len(left), len(right))):
        if ix >= len(left):
            return True
        elif ix >= len(right):
            return False
        elif isinstance(left[ix], list) and isinstance(right[ix], list):
            res = is_less_than(left[ix], right[ix])
            if res is not None:
                return res
        elif isinstance(left[ix], int) and isinstance(right[ix], list):
            res = is_less_than([left[ix]], right[ix])
            if res is not None:
                return res
        elif isinstance(left[ix], list) and isinstance(right[ix], int):
            res = is_less_than(left[ix], [right[ix]])
            if res is not None:
                return res
        elif left[ix] < right[ix]:
            return True
        elif right[ix] < left[ix]:
                return False
    return None

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n\n")

    answer = 0
    for ix in range(1, len(data) + 1):
        packets =[json.loads(pair) for pair in data[ix-1].split("\n")]
        if is_less_than(packets[0], packets[1]):
            answer += ix

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
