#!/usr/bin/env python3

import os, itertools

rocks = [
([0b1111000, 0b0111100, 0b0011110, 0b0001111],),
([0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010],
 [0b1110000, 0b0111000, 0b0011100, 0b0001110, 0b0000111],
 [0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010]),
([0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1110000, 0b0111000, 0b0011100, 0b0001110, 0b0000111]),
([0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001]),
([0b1100000, 0b0110000, 0b0011000, 0b0001100, 0b0000110, 0b0000011],
 [0b1100000, 0b0110000, 0b0011000, 0b0001100, 0b0000110, 0b0000011])
]

def draw_board(board, rock_mask):
    if not rock_mask:
        rock_mask = board
    for iy in range(len(board) - 1, 0, -1):
        print(("0000000"+bin(board[iy] | rock_mask[iy])[2:])[-7:].replace("0",".").replace("1","#"))
    print("-"*7)

def rock_mask(rock, rock_coords, length):
    mask = [0] * length
    for ix in range(len(rock)):
        mask[rock_coords[1] - ix] = rock[ix][rock_coords[0]]
    return mask

def check_collision(board, rock, rock_coords):
    return any([rock[ix][rock_coords[0]] & board[rock_coords[1] - ix] for ix in range(len(rock))])

def fix_rock(board, rock, rock_coords):
    for ix in range(len(rock)):
        board[rock_coords[1] - ix] = board[rock_coords[1] - ix] | rock[ix][rock_coords[0]]
    
    for ix in range(len(board) - 1, 0, -1):
        if board[ix] != 0:
            return board[:ix + 1]

def answer(input_file, interactive = False):
    with open(input_file, "r") as input_data:
        jet_directions = input_data.read()

    board = [0b1111111]
    jet_ix = 0
    rock_count = 0
    for rock in itertools.cycle(rocks):
        rock_count += 1
        if rock_count == 2023:
            break
        rock_coords = (2,len(board)+len(rock)+2)
        board.extend([0] * (len(rock) + 3))
        if interactive:
            draw_board(board, None)
        moving = True
        while moving:
            if interactive:
                mask = rock_mask(rock, rock_coords, len(board))
                draw_board(board, mask)
                input()
            jet_direction = -1 if jet_directions[jet_ix] == "<" else 1
            jet_ix += 1
            jet_ix = jet_ix % len(jet_directions)
            if((rock_coords[0] + jet_direction) >= 0) and ((rock_coords[0] + jet_direction) < len(rock[0])):
                if not check_collision(board, rock, (rock_coords[0] + jet_direction, rock_coords[1])):
                    rock_coords = (rock_coords[0]+ jet_direction, rock_coords[1])
            if interactive:
                mask = rock_mask(rock, rock_coords, len(board))
                draw_board(board, mask)
                input()
            if check_collision(board, rock, (rock_coords[0], rock_coords[1]-1)):
                board = fix_rock(board, rock, rock_coords)
                answer = len(board) - 1
                break
            rock_coords = (rock_coords[0], rock_coords[1]-1)

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
