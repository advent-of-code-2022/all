#!/usr/bin/env python3

import os, itertools

rocks = [
([0b1111000, 0b0111100, 0b0011110, 0b0001111],),
([0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010],
 [0b1110000, 0b0111000, 0b0011100, 0b0001110, 0b0000111],
 [0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010]),
([0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1110000, 0b0111000, 0b0011100, 0b0001110, 0b0000111]),
([0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001],
 [0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001]),
([0b1100000, 0b0110000, 0b0011000, 0b0001100, 0b0000110, 0b0000011],
 [0b1100000, 0b0110000, 0b0011000, 0b0001100, 0b0000110, 0b0000011])
]

first_three_moves = [
{
    "<<<": -2,
    "<<>": -1,
    "<><": -1,
    "><<": -1,
    "<>>": 1,
    "><>": 1,
    ">><": 0,
    ">>>": 1
},
{
    "<<<": -2,
    "<<>": -1,
    "<><": -1,
    "><<": -1,
    "<>>": 1,
    "><>": 1,
    ">><": 1,
    ">>>": 2
},
{
    "<<<": -2,
    "<<>": -1,
    "<><": -1,
    "><<": -1,
    "<>>": 1,
    "><>": 1,
    ">><": 1,
    ">>>": 2
},
{
    "<<<": -2,
    "<<>": -1,
    "<><": -1,
    "><<": -1,
    "<>>": 1,
    "><>": 1,
    ">><": 1,
    ">>>": 3
},
{
    "<<<": -2,
    "<<>": -1,
    "<><": -1,
    "><<": -1,
    "<>>": 1,
    "><>": 1,
    ">><": 1,
    ">>>": 3
},
]

def answer(input_file):
    with open(input_file, "r") as input_data:
        jet_directions = input_data.read()

    target_rock_count = 1000000000000
    board = [0b1111111]
    jet_ix = 0
    jet_ix_reset = False
    rock_count = 0
    row_removed_count = 0
    answer = 0
    moving = False
    rock_coords = None
    sequences = {}
    initial_height = 0
    initial_rocks = 0
    repeating_height = 0
    repeating_rocks = 0
    extra_height = 0
    extra_rocks = 0

    def rebase_board():
        nonlocal board
        for ix in range(len(board) - 1, 2, -1):
            if (board[ix] | board[ix-1] | board[ix-2]) == 127:
                board = board[ix-3:]
                return ix-3
        return 0

    def get_jet_directions(count):
        nonlocal jet_ix, jet_ix_reset
        length = len(jet_directions)
        if jet_ix + count >= length:
            jet_ix_reset = True
            temp = jet_directions * 2
            return temp[jet_ix:jet_ix+count], (jet_ix + count) % length
        return jet_directions[jet_ix:jet_ix + count], jet_ix + count

    def initial_move():
        nonlocal jet_ix, rock_coords, moving
        first_three_directions, jet_ix = get_jet_directions(3)
        rock_coords = (2 + first_three[first_three_directions],len(board)+len(rock)-1)
        board.extend([0] * len(rock))
        moving = True
        return jet_ix, rock_coords

    def check_collision(test_coords):
        nonlocal rock, board
        rows = len(rock)
        if rock[0][test_coords[0]] & board[test_coords[1]] != 0:
            return True
        if rows >= 2:
            if rock[1][test_coords[0]] & board[test_coords[1] - 1] != 0:
                return True
            if rows >= 3:
                if rock[2][test_coords[0]] & board[test_coords[1] - 2] != 0:
                    return True
                if rows == 4:
                    if rock[3][test_coords[0]] & board[test_coords[1] - 3] != 0:
                        return True
        return False

    def sideways_move():
        nonlocal rock_coords, jet_ix, rock
        jet_direction, jet_ix = get_jet_directions(1)
        jet_direction = -1 if jet_direction == "<" else 1
        if((rock_coords[0] + jet_direction) >= 0) and ((rock_coords[0] + jet_direction) < len(rock[0])):
            if not check_collision((rock_coords[0] + jet_direction, rock_coords[1])):
                rock_coords = (rock_coords[0]+ jet_direction, rock_coords[1])

    def downwards_move():
        nonlocal board, answer, moving, rock_coords, row_removed_count
        if check_collision((rock_coords[0], rock_coords[1]-1)):
            fix_rock()
            answer = row_removed_count + len(board) - 1
            moving = False
        else:
            rock_coords = (rock_coords[0], rock_coords[1]-1)

    def fix_rock():
        nonlocal board, rock, rock_coords
        for ix in range(len(rock)):
            board[rock_coords[1] - ix] = board[rock_coords[1] - ix] | rock[ix][rock_coords[0]]
        
        for ix in range(len(board) - 1, - 1, -1):
            if board[ix] != 0:
                board = board[:ix + 1]
                break

    def check_for_sequences():
        nonlocal sequences, initial_height, initial_rocks, repeating_height, repeating_rocks, extra_rocks
        if not (rock_id, jet_ix) in sequences:
            sequences[(rock_id, jet_ix)] = (rock_count, answer)
        else:
            initial_height = sequences[(rock_id, jet_ix)][1]
            initial_rocks = sequences[(rock_id, jet_ix)][0]
            repeating_height = answer - initial_height
            repeating_rocks = rock_count - initial_rocks
            extra_rocks = (target_rock_count - sequences[(rock_id, jet_ix)][0]) % (rock_count - sequences[(rock_id, jet_ix)][0])

    for rock_count in itertools.count(0):
        rock_id = rock_count % len(rocks)
        rock = rocks[rock_id]
        if jet_ix_reset:
            row_removed_count += rebase_board()
            check_for_sequences()
        if rock_count > 0 and rock_count == extra_rocks + initial_rocks + repeating_rocks:
            extra_height = answer - repeating_height - initial_height
            break
        jet_ix_reset = False
        first_three = first_three_moves[rock_count % len(rocks)]
        initial_move()
        while moving:
            sideways_move()
            downwards_move()

    repetition_count = ((target_rock_count - initial_rocks) // repeating_rocks)
    answer = initial_height + (repeating_height * repetition_count) + extra_height

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
